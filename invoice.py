# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta

__all__ = ['InvoiceLine']


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @classmethod
    def search_office(cls, name, clause):
        res = super().search_office(name, clause)
        res.append(
            ('origin.%s' % clause[0], ) + tuple(clause[1:]) + (
                'purchase.line', )
        )
        return res
