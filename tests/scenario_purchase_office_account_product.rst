========================================
Purchase Office Account Product Scenario
========================================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import \
    ...     create_chart, get_accounts
    >>> from decimal import Decimal


Install office_account_product::

    >>> config = activate_modules(['purchase_office',
    ...     'account_office', 'office_account_product'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Get user::

    >>> User = Model.get('res.user')
    >>> admin = User(config.user)


Create offices::

    >>> Office = Model.get('company.office')
    >>> office1 = Office()
    >>> office1.name = 'Office 1'
    >>> office1.company = company
    >>> office1.save()
    >>> office2 = Office()
    >>> office2.name = 'Office 2'
    >>> office2.company = company
    >>> office2.save()


Add office to admin user and reload context::

    >>> admin.offices.append(office1)
    >>> office1 = Office(office1.id)
    >>> admin.offices.append(office2)
    >>> office2 = Office(office2.id)
    >>> admin.office = office1
    >>> admin.save()
    >>> config._context = User.get_preferences(True, config.context)


Create accounts::

    >>> Account = Model.get('account.account')
    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> payable = accounts['payable']
    >>> expense = accounts['expense']
    >>> expense2, = expense.duplicate()
    >>> expense2.name = 'Expense 2'
    >>> expense2.save()


Create supplier::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.account_payable = payable
    >>> supplier.save()


Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name='Account Category')
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.save()
    >>> admin.office = office2
    >>> admin.save()
    >>> config._context = User.get_preferences(True, config.context)
    >>> account_category = ProductCategory(account_category.id)
    >>> account_category.account_expense = expense2
    >>> account_category.save()
    >>> admin.office = office1
    >>> admin.save()
    >>> config._context = User.get_preferences(True, config.context)


Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'Product'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.purchasable = True
    >>> template.list_price = Decimal('50')
    >>> template.cost_price = Decimal('20')
    >>> template.account_category = account_category
    >>> template.save()
    >>> product = template.products[0]


Add office to journal::

    >>> Journal = Model.get('account.journal')
    >>> journal_expense, = Journal.find([('code', '=', 'EXP')])
    >>> journal_expense.offices.append(office1)
    >>> office1 = Office(office1.id)
    >>> journal_expense.offices.append(office2)
    >>> office2 = Office(office2.id)
    >>> journal_expense.save()


Create purchase::

    >>> Purchase = Model.get('purchase.purchase')
    >>> purchase = Purchase()
    >>> purchase.party = supplier
    >>> purchase.invoice_method = 'order'
    >>> purchase_line = purchase.lines.new()
    >>> purchase_line.product = product
    >>> purchase_line.quantity = 1.0
    >>> purchase_line.unit_price = Decimal('5.0')
    >>> for st in ['quote', 'confirm', 'process']:
    ...     purchase.click(st)


Check invoice::

    >>> Invoice = Model.get('account.invoice')
    >>> invoice, = Invoice.find([])
    >>> iline, = invoice.lines
    >>> iline.account == expense
    True


Create purchase with office2::

    >>> purchase2 = Purchase()
    >>> purchase2.office = office2
    >>> purchase2.party = supplier
    >>> purchase2.invoice_method = 'order'
    >>> purchase_line = purchase2.lines.new()
    >>> purchase_line.product = product
    >>> purchase_line.quantity = 1.0
    >>> purchase_line.unit_price = Decimal('5.0')
    >>> for st in ['quote', 'confirm', 'process']:
    ...     purchase2.click(st)


Check invoice::

    >>> invoice2, = Invoice.find(['office', '=', office2])
    >>> iline2, = invoice2.lines
    >>> iline2.account == expense2
    True