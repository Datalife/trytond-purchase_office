========================
Purchase Office Sequence
========================

Imports::

    >>> import datetime
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> today = datetime.date.today()


Install purchase_office::

    >>> config = activate_modules(['purchase_office', 'office_sequence'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Create party::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier')
    >>> supplier.save()


Create branch office::

    >>> Office = Model.get('company.office')
    >>> office1 = Office(name='Office 1')
    >>> office1.save()
    >>> office2 = Office(name='Office 2')
    >>> office2.save()
    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> user = User(config.user)
    >>> user.offices.extend([office1, office2])
    >>> office1 = Office(office1.id)
    >>> user.office = office1
    >>> user.save()


Add period to purchase sequence::

    >>> PurchaseConf = Model.get('purchase.configuration')
    >>> conf = PurchaseConf(1)
    >>> sequence = conf.purchase_sequence
    >>> period = sequence.periods.new()
    >>> period.start_date = datetime.date(today.year, 1, 1)
    >>> period.end_date = datetime.date(today.year, 12, 31)
    >>> period.prefix = 'POF1/'
    >>> period.office = office1
    >>> period2 = sequence.periods.new()
    >>> period2.start_date = datetime.date(today.year, 1, 1)
    >>> period2.end_date = datetime.date(today.year, 12, 31)
    >>> period2.prefix = 'POF2/'
    >>> period2.office = office2
    >>> sequence.save()


Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')

    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.purchasable = True
    >>> template.list_price = Decimal('10')
    >>> template.save()
    >>> product, = template.products


Create purchases::

    >>> Purchase = Model.get('purchase.purchase')
    >>> purchase = Purchase()
    >>> purchase.purchase_date = today
    >>> purchase.office = office1
    >>> purchase.party = supplier
    >>> purchase.invoice_method = 'order'
    >>> purchase_line = purchase.lines.new()
    >>> purchase_line.product = product
    >>> purchase_line.quantity = 2.0
    >>> purchase.save()
    >>> purchase.click('quote')
    >>> purchase.number
    'POF1/1'
    >>> purchase2 = Purchase()
    >>> purchase2.purchase_date = today
    >>> purchase2.office = office2
    >>> purchase2.party = supplier
    >>> purchase2.invoice_method = 'order'
    >>> purchase_line = purchase2.lines.new()
    >>> purchase_line.product = product
    >>> purchase_line.quantity = 2.0
    >>> purchase2.save()
    >>> purchase2.click('quote')
    >>> purchase2.number
    'POF2/1'